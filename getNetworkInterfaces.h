#ifndef GETNETWORKINTERFACES
#define GETNETWORKINTERFACES

#include <fstream>
#include <string>
#include <vector>

std::vector <std::string> getNetworkInterfaces ();

#endif // GETNETWORKINTERFACES
