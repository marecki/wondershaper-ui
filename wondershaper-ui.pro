#-------------------------------------------------
#
# Project created by QtCreator 2015-08-21T11:56:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = wondershaper-ui
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    getNetworkInterfaces.cpp

HEADERS  += mainwindow.h \
    getNetworkInterfaces.h

FORMS    += mainwindow.ui

CONFIG += c++11

DISTFILES += \
    LICENSE \
    README.md
