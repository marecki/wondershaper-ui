#include "getNetworkInterfaces.h"

std::vector <std::string> getNetworkInterfaces () {
    std::ifstream file;

    file.open("/proc/net/dev");

    int cnt = 0;

    std::vector <std::string> list;

    for(std::string line; getline( file, line ); cnt++)
    {
        if (cnt > 1) {
            std::size_t pos = line.find_first_of(":");
            line.resize(pos);
            std::size_t pos2 = line.find_first_not_of(" ");
            line.erase(0,pos2);
            list.push_back(line);
        }
    }

    file.close();

    return list;
}
