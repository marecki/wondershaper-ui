#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "getNetworkInterfaces.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto list = getNetworkInterfaces();

    for (auto i : list) {
        QString iface = QString::fromStdString(i);
        ui->comboBoxInterfaces->addItem(iface);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_setButton_clicked()
{
    unsigned int dSpeed = ui->dLimit->text().toInt();
    unsigned int uSpeed = ui->uLimit->text().toInt();
    if (dSpeed > 0 && uSpeed > 0) {
        std::string command = "gksudo wondershaper " + ui->comboBoxInterfaces->currentText().toStdString() + " " + std::to_string(dSpeed) + " " + std::to_string(uSpeed);
        FILE* setCommand = popen(command.c_str(), "r");
        pclose(setCommand);
        std::string message = "Speed limits on interface " + ui->comboBoxInterfaces->currentText().toStdString() + " have been set to " + std::to_string(dSpeed) + "kbps download and " + std::to_string(uSpeed) + "kbps upload.";
        QMessageBox::information(this,"Limits have been set",message.c_str());
    } else {
        std::string message = "There was a problem and limits were not set. Keep in mind that values of download and upload limit should be both more than zero.";
        QMessageBox::information(this,"Problem",message.c_str());
    }
}

void MainWindow::on_resetButton_clicked()
{
    std::string command = "gksudo wondershaper clear " + ui->comboBoxInterfaces->currentText().toStdString();
    FILE* resetCommand = popen(command.c_str(), "r");
    pclose(resetCommand);
    std::string message = "Speed limits on interface " + ui->comboBoxInterfaces->currentText().toStdString() + " have been cleared.";
    QMessageBox::information(this,"Limits have been cleared",message.c_str());
}
