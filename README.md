![wondershaper-ui.png](https://bitbucket.org/repo/byk5kp/images/842230660-wondershaper-ui.png)
# README #

### Description ###

* This is simple UI for [wondershaper](http://lartc.org/wondershaper/) application
* Wondershaper is a tool for limiting bandwidth on a particular network interface

### Version ###

This is version 0.00001.

### Requirements ###

* Ubuntu 14.04+ or any modern linux distribution
* wondershaper package installed

### Building on Ubuntu ###

+ Install dependencies for building
```
#!bash

sudo apt-get install build-essential libgl1-mesa-dev qt5-qmake qt5-default
```
+ Obtain this repository

```
#!bash

git clone git@bitbucket.org:marecki/wondershaper-ui.git
```
+ Go to wondershaper-ui/ directory and execute these commands:

```
#!bash
qmake wondershaper-ui.pro -r -spec linux-g++-64 CONFIG+=debug && \
make
```
+ Run wondershaper-ui executable by double-clicking or

```
#!bash

./wondershaper-ui
```